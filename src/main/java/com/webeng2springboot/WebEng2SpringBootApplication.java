package com.webeng2springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebEng2SpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebEng2SpringBootApplication.class, args);
    }

}
